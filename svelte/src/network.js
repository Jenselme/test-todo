export const loadTodos = () => {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve([
                { id: 1, title: "Do a Svelte app", done: false },
                { id: 2, title: "Style the app", done: false },
            ])
        }, 3000);
    });
}