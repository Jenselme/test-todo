(ns testing-reframe.events
  (:require
   [re-frame.core :as re-frame]
   [testing-reframe.db :as db]
   [ajax.core :as ajax]
   [day8.re-frame.http-fx]))

(re-frame/reg-event-db
 ::initialize-db
 (fn [_ _]
   db/default-db))

(re-frame/reg-event-fx
 ::delete-todo
 (fn [coeffects effects]
   (let [db (:db coeffects)
         todo-id (second effects)
         new-db (update-in db [:todos] (fn [todos] (filterv #(not= (:id %) todo-id) todos)))]
     {:db new-db})))

(re-frame/reg-event-fx
 ::add-todo
 (fn [coeffects effects]
   (let [db (:db coeffects)
         new-id (inc (apply max (map #(:id %) (:todos db))))
         todo (second effects)
         todo-with-id (assoc todo :id new-id)
         new-db (update-in db [:todos] #(conj % todo-with-id))]
     {:db new-db})))

(re-frame/reg-event-fx
 ::toggle-done
 (fn [coeffects effects]
   (let [db (:db coeffects)
         todo (second effects)
         new-todo (assoc todo :done (not (:done todo)))
         new-db (update-in db [:todos]
                           (fn [todos] (mapv #(if (= (:id %) (:id todo)) new-todo %) todos)))]
     {:db new-db})))

(defn handle-response
  [db]
  (assoc db
         :todos [{:id 1 :title "Write the app" :done false}
                 {:id 2 :title "Style the app" :done false}]
         :loading? false))

(re-frame/reg-event-fx
 ::process-response
 (fn [coeffects effects]
   (let [db (:db coeffects)]
   {:db (handle-response db)})))

(re-frame/reg-event-fx
 ::bad-response
 (fn [coeffects effects]
   (let [db (:db coeffects)]
     {:db (handle-response db)})))

; See https://github.com/day8/re-frame/blob/master/docs/Talking-To-Servers.md#version-2
(re-frame/reg-event-fx
 ::fetch-todos
 (fn
   [{db :db} _]

    ;; we return a map of (side) effects
   {:http-xhrio {:method          :get
                 :uri             "https://www.jujens.eu"
                 :format          (ajax/json-request-format)
                 :response-format (ajax/json-response-format {:keywords? true})
                 :on-success      [::process-response]
                 :on-failure      [::bad-response]}
    :db  (assoc db :loading? true)}))