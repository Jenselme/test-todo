(ns testing-reframe.views
  (:require
   [clojure.string :as s]
   [reagent.core :as reagent]
   [re-frame.core :as re-frame]
   [testing-reframe.subs :as subs]
   [testing-reframe.events :as events]))

(def empty-todo {:title "" :done false})


(defn display-todo
  [todo]
  [:p
   {:style {:text-decoration (if (:done todo) "line-through" "inherit")}}
   (:title todo)
   [:button
    {:type "button"
     :on-click #(re-frame.core/dispatch [::events/delete-todo (:id todo)])}
    "Delete todo"]
   [:button
    {:type "button"
     :on-click #(re-frame.core/dispatch [::events/toggle-done todo])}
    (if (:done todo) "Mark as not done" "Mark as done")]])

(defn update-todo-title
  [todo new-title]
  (assoc todo :title new-title))

(defn update-todo-done
  [todo status]
  (assoc todo :done status))

(defn add-todo
  []
  (let [todo (reagent/atom empty-todo)]
    (fn []
      [:form
       {:on-submit #((.preventDefault %)
                     (when (not (s/blank? (:title @todo)))
                       ((reset! todo empty-todo)
                        (re-frame.core/dispatch [::events/add-todo @todo]))))}
       [:div
        [:label {:for "new-todo-title"} "New TODO title"]
        [:input {:id "new-todo-title"
                 :type "text"
                 :placeholder "TODO title"
                 :on-change #(swap! todo update-todo-title (-> % .-target .-value))
                 :value (:title @todo)}]]
       [:div
        [:label {:for "new-todo-done"} "Is done?"]
        [:input {:id "new-todo-done"
                 :type "checkbox"
                 :on-change #(swap! todo update-todo-done (-> % .-target .-checked))
                 :value (:done @todo)}]]
       [:button {:type "submit"} "Add TODO"]])))

(defn main-panel []
  (let [todos (re-frame/subscribe [::subs/todos])
        loading? (re-frame/subscribe [::subs/loading?])]
    [:div
     [:h1
      "My TODOs"]
     (if @loading?
       [:p "Loading"]
       [:ul
        (for [todo @todos] ^{:key (:id todo)} [:li [display-todo todo]])])
     [add-todo]]))

(defn app []
  (with-meta main-panel {:component-did-mount (js/setTimeout #(re-frame/dispatch [::events/fetch-todos]) 3000)}))

