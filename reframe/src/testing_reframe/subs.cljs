(ns testing-reframe.subs
  (:require
   [re-frame.core :as re-frame]))

(re-frame/reg-sub
 ::todos
 (fn [db]
   (:todos db)))

(re-frame/reg-sub
 ::loading?
 (fn [db]
   (:loading? db)))
